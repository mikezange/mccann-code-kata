<?php

Class Numerals{
    
    private $mapping = [
        'M' => '1000',
        'CM' => '900',
        'D' => '500',
        'CD' => '400',
        'C' => '100',
        'XC' => '90',
        'L' => '50',
        'XL' => '40',
        'X' => '10',
        'IX' => '9',
        'V' => '5',
        'IV' => '4',
        'I' => '1',
    ];
    
    private $integers;
    private $numerals;
    
    
    public function __construct(){
        
        $this->numerals = [];
        
    }
    
    
    public function getNumerals($input){
        
        $input = $this->cleanString($input);
        
        if(strpos($input, ',')){
            $this->integers = explode(',', $input);
        }else{
            $this->integers = [$input];
        }
        
        foreach($this->integers as $integer){
			
            $numeral = $this->toNumeral($integer);
			
            $output_string = $integer . ' = "' . $numeral .'"';
			
            array_push($this->numerals, $output_string);
			
        }

        return $this->numerals;
        
    }
    
    
    private function toNumeral($int){
        
        if(!$this->isNumber($int)){
            return "NaN";
        }
        
        if($int >= 4000 || $int <= 0){
            return "Number out of bounds";
        }

        $numeral = "";
		
        while($int > 0){
            foreach($this->mapping as $key => $value){
                if($int >= $value){
                    $numeral .= $key;
                    $int = $int - $value;
                    break;
                }
            }
        }
        
        return (string)$numeral;
        
    }
    
    
    private function cleanString($input){
        
        $stripped = preg_replace('/\s+/', '', $input);
        
        $stripped = trim($stripped, ',');
		
        $stripped = str_replace(",,", ",", $stripped);

        return $stripped;
		
    }
    
    
    private function isNumber($int){

        return (is_numeric($int) === true ? true : false);
		
    }
    
}

$model = new Numerals();

echo "\n\nPlease enter an integer or a string of integers seperated by comma and press <enter>\n\n";

echo "Input: ";

$handle = fopen ("php://stdin","r");

$line = trim(fgets($handle));

echo "\nOutput:\n";

foreach($model->getNumerals($line) as $item){
    echo $item . "\n";
}

fclose($handle);

exit;
